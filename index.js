const token = "xoxb-620957299781-609587828226-lCsgvq703rqhW6JrOQFByHlR";
const botID = "<@UHXH9QC6N>";
const channelID = 'CJ93H1SPJ';


const { RTMClient } = require('@slack/rtm-api');

const DomainManagement = require('./domain-management');
const domainManagementInstance = new DomainManagement();

const helperClass = require('./functions');
const helper = new helperClass();

// Initialize
const rtm = new RTMClient(token);

// SEND WELCOME MESSAGE TO THE CHANNEL.
rtm.sendMessage('Hello, the bot is active', channelID);

// Universal object which contains methods of how to react on user's input (dialog mode)
var inputHandler = { };

// Accepting domain name to be added
inputHandler.awaitForDomainName = function(event) {
    let newDomain = helper.extractHostname(event.text);

    if(newDomain === null){
        let message = "I can't recognize this domain";
        const reply =  rtm.sendMessage(message, event.channel);
        return false;
    }

    let addDomain = async() => {
        let status = await domainManagementInstance.addDomain(newDomain);
        let message = "Domain *" + newDomain + '* was ' + ( (status)?"added":"ignored" );
        const reply =  rtm.sendMessage(message, event.channel);
    }
    addDomain();
}

// Accepting domain ID to be removed.
inputHandler.awaitForDomainNameToBeRemoved = function(event){
    let action = async() => {
        let domain = await domainManagementInstance.getDomainNameByID(event.text);
        let message = "Confirm *" + domain.domain_name + "* to be removed? (yes / no)";
        const reply =  rtm.sendMessage(message, event.channel);
        helper.writeToFile(event.user, 'awaitForDomainNameToBeRemovedConfirmation:'+event.text);
    }
    action();
}

// Accepting confirmation to remove domain.
inputHandler.awaitForDomainNameToBeRemovedConfirmation = function(event, domainID){
    if(event.text.toLowerCase().trim() == 'yes' ) {
        let action = async() => {
            let domainRealKey = await domainManagementInstance.getRealDomainID(domainID)
            let status = await domainManagementInstance.removeDomain(domainRealKey);
            let message = 'Domain removed successfully';
            const reply =  rtm.sendMessage(message, event.channel);
        }
        action();
    } else {
        let message = 'Ok, no problems.';
        const reply =  rtm.sendMessage(message, event.channel);
        helper.writeToFile(event.user, '');
    }
}

// Check if bot awaits some feedback from the user.
let promptInputActionFunc = async(event) => {
    // I store function name and parmeter in the /tmp/@UserID file
    let userInput = await helper.checkAwaitingForUserInput(event.user);
    let stringArgs = userInput.split(':');

    let action = stringArgs[0];
    let argument = stringArgs[1];

    // Call action after user's input.
    if(action.trim() != ''){
        helper.writeToFile(event.user, '');
        inputHandler[action](event, argument);
    }
}

// Get domains from the firebase.
let getMyDomains = async() => {
    let allDomains = await domainManagementInstance.listDomains();

    return new Promise(resolve => {
        let myDomainsArray = [];
        if (allDomains !== null && typeof allDomains !== "undefined") {
            Object.keys(allDomains).map(function (objectKey, index) {
                myDomainsArray.push(allDomains[objectKey]);
            });
            resolve(myDomainsArray);
        } else {
            resolve(false);
        }
    });
}

// Lister on message events.
rtm.on('message', (event) => {

    // If message contains @botName, then perform further processing.
    if(event.type=="message" && event.text && event.text.includes(botID)) {

        // Just to test if bot is active.
        if(event.text.includes("test")){
            console.log(event.channel);
            const reply =  rtm.sendMessage(`Hi, <@${event.user}>, I'm listening you`, event.channel);
            return false;
        }

        // LIST DOMAINS
        if(event.text.includes("list domains")){
            let domains =async() => {
                await getMyDomains().then((response) => {
                    let message;

                    if(response == false){
                        message = 'We don\'t have any domains in the DB \n\n';
                    } else {
                        message = 'Curretly, we monitor such domains: \n\n';
                        response.map(function (domain, index) {
                            message = message + index + ' - ' + domain.domain_name + '\n'
                        });
                    }
                    message = message + helper.getAvailableCommands();
                    const reply =  rtm.sendMessage(message, event.channel);
                });
            }
            domains();
            return false;
        }

        // ADD DOMAIN
        if(event.text.includes("add domain")){
            const reply =  rtm.sendMessage('Please enter the domain name you want to add', event.channel);
            helper.writeToFile(event.user, 'awaitForDomainName');
            return false;
        }

        // REMOVE DOMAIN
        if(event.text.includes("remove domain")){
            helper.writeToFile(event.user, 'awaitForDomainNameToBeRemoved');

            let domains = async() => {
                await getMyDomains().then((response) => {
                    let message =
                        'Please choose domain ID to be removed.\n\n'
                        + 'Domains list: \n\n';
                    response.map(function(domain, index){
                        message = message + '[' +index + ']' + ' - ' + domain.domain_name + '\n'
                    });
                    const reply =  rtm.sendMessage(message, event.channel);
                });
            }
            domains();
            return false;
        }

        // For all other texts with bot mentioned, just provide a list of commands available.
        if(event.text){
            let message = `You've sad: *` + event.text.replace(botID, '') + '*' + helper.getAvailableCommands();
            const reply =  rtm.sendMessage(message, event.channel);
            return false;
        }
    } else {
        // Check user's answer for previos input request.
        promptInputActionFunc(event);
    }
});

// THIS SECTION WAS DESIGNED FOR SERVICE PURPOSES, TO CHECK IF SITE IS LIVE OR NOT.

const ping = require('ping');
var schedule = require('node-schedule');
var j = schedule.scheduleJob('* * * * *', function(){

    let cron = async() => {
        await getMyDomains().then((response) => {
            response.map(function(domain, index){
                ping.sys.probe(domain.domain_name, function(isAlive){
                    if(!isAlive){
                        const reply =  rtm.sendMessage(':warning: The host ' + domain.domain_name + ' is dead :skull_and_crossbones:', channelID);
                    }
                    console.log('checked', domain.domain_name, isAlive);
                });
            });

        });
    }
    cron();

    return false;
});

// Start channel listing.
(async () => {
    await rtm.start();
})();
