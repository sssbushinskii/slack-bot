const axios = require('axios');

const instance = axios.create({
    baseURL: 'https://react-6457e.firebaseio.com/',
});

class DomainManagement {
    getDate() {
        let currentdate = new Date();
        let datetime = currentdate.getDate() + "/"
            + (currentdate.getMonth() + 1) + "/"
            + currentdate.getFullYear() + " @ "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();
        return datetime;
    }

    async addDomain(domainName) {
        let status;
        let domainExists = await this.checkDomainExists(domainName);
        if (domainExists) {
            status = false;
        } else {
            let domain = {
                date_added: this.getDate(),
                domain_name: domainName.trim().toLowerCase()
            };
            await instance.post('/domains.json', domain)
                .then(response => {
                    status = true;
                })
                .catch(error => {

                });
        }
        return status;
    }

    async checkDomainExists(domainName) {
        let existedDomains = await this.getDomains();
        let domainNameToCheck = domainName = domainName.trim().toLowerCase();

        if (existedDomains === null) {
            return false;
        }

        let domainExists = false;
        Object.keys(existedDomains).map(function (objectKey, index) {
            var value = existedDomains[objectKey];
            if (domainNameToCheck == value.domain_name) {
                domainExists = true;
            }
        });
        return domainExists;
    }

    async getDomains() {
        let data = await instance.get('/domains.json')
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(error);
            });
        return data;
    }

    async getRealDomainID(domainIndex) {
        let allDomains = await this.getDomains();
        let domainKey;
        Object.keys(allDomains).map(function (objectKey, index) {
            var value = allDomains[objectKey];
            if (domainIndex == index) {
                domainKey = objectKey;
            }
        });
        return domainKey;
    }

    async getDomainNameByID(id) {
        let allDomains = await this.getDomains();
        let domainName;
        Object.keys(allDomains).map(function (objectKey, index) {
            var value = allDomains[objectKey];
            if (index == id) {
                domainName = value;
            }
        });
        return domainName;
    }

    async listDomains() {
        let domains = await this.getDomains();
        return domains;
    }

    async removeDomain(domainID) {
        let res = await instance.delete('/domains/' + domainID + '.json').then(response => {
            return response;
        });
    }
}
module.exports = DomainManagement;

