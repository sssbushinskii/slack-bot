const fs = require('fs');

class Helper {

    extractHostname(url) {
        var hostname;
        //find & remove protocol (http, ftp, etc.) and get hostname

        if (url.indexOf("//") > -1) {
            hostname = url.split('/')[2];
        }
        else {
            hostname = url.split('/')[0];
        }

        //find & remove port number
        hostname = hostname.split(':')[0];
        //find & remove "?"
        hostname = hostname.split('?')[0];
        hostname = hostname.split(('|'))[0];
        return hostname;
    }

    writeToFile(userID, $text){
        let path = "tmp/@"+userID;

        fs.writeFile(path, $text, function(err) {

        });
    }

    async checkAwaitingForUserInput(userID){
        let path = "tmp/@"+userID;
        try {
            if (fs.existsSync(path)) {
                return new Promise(resolve => {
                    fs.access('tmp/@'+userID, fs.constants.F_OK | fs.constants.W_OK, (err) => {
                        if (err) {
                            console.log('error', err);
                            resolve(false);
                        } else {
                            fs.readFile('tmp/@'+userID, "utf8", (err, data) => {
                                if (err) throw err;
                                resolve(data);
                            });
                        }
                    });
                });
            }
        } catch(err) {
            console.error(err)
        }
        return false;
    }



    getAvailableCommands() {
        return '\n*********\nAvailable commands: \n' +
            '*add domain*\n' +
            '*remove domain*\n' +
            '*list domains*\n';
    }


}

module.exports = Helper;